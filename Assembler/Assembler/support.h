﻿#pragma once
#include <string>
#include <iostream>
std::string& ltrim(std::string& str, const std::string& chars = "\t\n\v\f\r ")
{
	str.erase(0, str.find_first_not_of(chars));
	return str;
}

std::string& rtrim(std::string& str, const std::string& chars = "\t\n\v\f\r ")
{
	str.erase(str.find_last_not_of(chars) + 1);
	return str;
}

std::string& trim(std::string& str, const std::string& chars = "\t\n\v\f\r ")
{
	return ltrim(rtrim(str, chars), chars);
}
void title()
{
	std::cout << "\t\t   /$$$$$$                                             /$$       /$$                     \n" <<
		"\t\t  /$$__  $$                                           | $$      | $$                     \n" <<
		"\t\t | $$  \\ $$  /$$$$$$$ /$$$$$$$  /$$$$$$  /$$$$$$/$$$$ | $$$$$$$ | $$  /$$$$$$   /$$$$$$  \n" <<
		"\t\t | $$$$$$$$ /$$_____//$$_____/ /$$__  $$| $$_  $$_  $$| $$__  $$| $$ /$$__  $$ /$$__  $$ \n" <<
		"\t\t | $$__  $$|  $$$$$$|  $$$$$$ | $$$$$$$$| $$ \\ $$ \\ $$| $$  \\ $$| $$| $$$$$$$$| $$  \\__/ \n" <<
		"\t\t | $$  | $$ \\____  $$\\____  $$| $$_____/| $$ | $$ | $$| $$  | $$| $$| $$_____/| $$       \n" <<
		"\t\t | $$  | $$ /$$$$$$$//$$$$$$$/|  $$$$$$$| $$ | $$ | $$| $$$$$$$/| $$|  $$$$$$$| $$             \n" <<
		"\t\t |__/  |__/|_______/|_______/  \\_______/|__/ |__/ |__/|_______/ |__/ \\_______/|__/             \n" <<
		"\t\t                                                                                                 \n" <<
		"\t\t   [+] Coded By H.S & P.R [+]   [+] ID1:973613035 [+]   [+] ID2:973613025 [+]\n" << std::endl;
	std::cout << std::endl << std::endl;
}