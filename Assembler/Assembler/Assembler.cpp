#include "Assembler.h"
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

void Assembler::assemble(std::string arg_source, std::string arg_des)
{
	this->loadInstructions(arg_source, arg_des);
	this->fillSymbolTable();
	this->placeValueOfSymbols();
	this->printIns();
	this->assembleInstruction();
	std::cout << std::endl << std::endl;
	this->print();
	this->writeToFile();
}

void Assembler::createMap(std::unordered_map<std::string, char>* um)
{
	(*um)["0000"] = '0';
	(*um)["0001"] = '1';
	(*um)["0010"] = '2';
	(*um)["0011"] = '3';
	(*um)["0100"] = '4';
	(*um)["0101"] = '5';
	(*um)["0110"] = '6';
	(*um)["0111"] = '7';
	(*um)["1000"] = '8';
	(*um)["1001"] = '9';
	(*um)["1010"] = 'A';
	(*um)["1011"] = 'B';
	(*um)["1100"] = 'C';
	(*um)["1101"] = 'D';
	(*um)["1110"] = 'E';
	(*um)["1111"] = 'F';
}

std::string Assembler::convertBinToHex(std::string bin)
{
	int l = bin.size();
	int t = bin.find_first_of('.');

	int len_left = t != -1 ? t : l;
 
	for (int i = 1; i <= (4 - len_left % 4) % 4; i++)
		bin = '0' + bin;

	if (t != -1)
	{
		int len_right = l - len_left - 1;

		for (int i = 1; i <= (4 - len_right % 4) % 4; i++)
			bin = bin + '0';
	}

	std::unordered_map<std::string, char> bin_hex_map;
	createMap(&bin_hex_map);

	int i = 0;
	std::string hex = "";

	while (1)
	{
		hex += bin_hex_map[bin.substr(i, 4)];
		i += 4;
		if (i == bin.size())
			break;

		if (bin.at(i) == '.')
		{
			hex += '.';
			i++;
		}
	}
	return hex;
}
std::string Assembler::decimalToBinary(int arg_digit)
{
	std::string binary = "";
	if (arg_digit < 0)
	{
		this->sign = false;
		arg_digit *= -1;
	}
	while (arg_digit > 0)
	{
		binary.insert(0, std::to_string(arg_digit % 2));
		arg_digit /= 2;
	}
	int lenght = 8 - binary.length();
	for (int i = 0; i < lenght; i++)
	{
		binary.insert(0, "0");
	}

	if (!this->sign)
	{
		binary = this->twoComplement(binary);
		this->sign = true;
	}
	return binary;
}

std::string Assembler::expansion(std::string arg_bin, int arg_expan)
{
	int loop = arg_expan - arg_bin.size(), a = arg_bin.length();
	std::string sign;
	if (this->sign)
		sign = '0';
	else
		sign = '1';

	for (int i = 0; i < loop; i++)
	{
		arg_bin.insert(0, sign);
	}
	return arg_bin;
}

void Assembler::assembleInstruction()
{
	int pc = 0;
	for (auto instLine = this->_instructionVector.begin(); instLine != this->_instructionVector.end(); instLine++)
	{
		std::istringstream iss(*instLine);
		std::string inst, binaryIns = "";
		iss >> inst;
		if (inst == "add" or inst == "sub" or inst == "slt" or inst == "or" or inst == "and" or inst == "jr")
			binaryIns += this->rTypeIns(inst, iss);

		else if (inst == "addi" or inst == "lw" or inst == "sw" or inst == "beq" or inst == "rol")
			binaryIns += this->iTypeIns(inst, iss, pc);

		else if (inst == "j" or inst == "jal")
			binaryIns += this->jTypeIns(inst, iss);
		else
			exit(1);
		this->_bineryConIns.push_back(binaryIns);
		pc ++;
	}

}

std::string Assembler::twoComplement(std::string arg_binary)
{
	for (auto itr = arg_binary.begin(); itr != arg_binary.end(); itr++)
	{
		if (*itr == '0')
			*itr = '1';
		else
			*itr = '0';
	}
	for (auto itr = arg_binary.rbegin(); itr != arg_binary.rend(); itr++)
	{
		if (*itr == '0')
		{
			*itr = '1';
			break;
		}
		else
		{
			*itr = '0';
		}
	}

	return arg_binary;
}
std::string Assembler::rTypeConvertor(int arg_digit)
{
	std::string binary = "";
	if (arg_digit == 0)
	{
		binary = "00";
	}
	else if (arg_digit == 1)
	{
		binary = "01";
	}
	else if (arg_digit == 2)
	{
		binary = "10";
	}
	else
	{
		binary = "11";
	}
	return binary;
}
std::string Assembler::rTypeIns(std::string arg_ins, std::istringstream& arg_remaining)
{
	std::string result = "", rs, rt, rd;
	std::getline(arg_remaining, rd, ',');
	std::getline(arg_remaining, rs, ',');
	std::getline(arg_remaining, rt, ',');
	result += this->__instruOpCode[arg_ins];
	result += this->rTypeConvertor(std::stoi(rs));
	result += this->rTypeConvertor(std::stoi(rt));
	result += this->rTypeConvertor(std::stoi(rd));
	result += this->__rTypeFounc[arg_ins];
	return result;
}

std::string Assembler::iTypeIns(std::string arg_ins, std::istringstream& arg_remaining, int pc)
{
	std::string result = "", rs, rt, imm;
	if (arg_ins == "lw" or arg_ins == "sw")
	{
		std::getline(arg_remaining, rt, ',');
		std::getline(arg_remaining, rs, ',');
		std::getline(arg_remaining, imm, ',');
		result += this->__instruOpCode[arg_ins];
		result += this->rTypeConvertor(std::stoi(rs));
		result += this->rTypeConvertor(std::stoi(rt));
		result += this->decimalToBinary(std::stoi(imm));
	}
	else if(arg_ins == "addi" or arg_ins == "rol")
	{
		std::getline(arg_remaining, rs, ',');
		std::getline(arg_remaining, rt, ',');
		std::getline(arg_remaining, imm, ',');
		result += this->__instruOpCode[arg_ins];
		result += this->rTypeConvertor(std::stoi(rs));
		result += this->rTypeConvertor(std::stoi(rt));
		result += this->decimalToBinary(std::stoi(imm));
	}
	else if(arg_ins == "beq")
	{
		std::getline(arg_remaining, rs, ',');
		std::getline(arg_remaining, rt, ',');
		std::getline(arg_remaining, imm, ',');
		result += this->__instruOpCode[arg_ins];
		result += this->rTypeConvertor(std::stoi(rs));
		result += this->rTypeConvertor(std::stoi(rt));
		result += this->decimalToBinary(std::stoi(imm));
	}
	return result;
}

std::string Assembler::jTypeIns(std::string arg_ins, std::istringstream& arg_remaining)
{
	std::string result = "", targetAddr;
	arg_remaining >> targetAddr;
	result += this->__instruOpCode[arg_ins] + this->expansion(this->decimalToBinary(std::stoi(targetAddr)), 12);
	return (result);
}

void Assembler::writeToFile()
{
	std::ofstream destination(this->fileDestination, std::ios::out);
	destination << "v2.0 raw" << std::endl;
	for (auto i : this->_bineryConIns)
	{
		destination << this->convertBinToHex(i) << std::endl;
	}
	destination.close();
}

void Assembler::print()
{
	std::cout << "   [2]Binary:" << std::endl;
	for (auto code : this->_bineryConIns)
	{
		std::cout << "\t" << code << std::endl;
	}
}
