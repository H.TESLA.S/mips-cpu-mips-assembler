#pragma once
#include "SymbolScanner.h"
#include <vector>
#include <string>
#include <unordered_map>
class Assembler :
	public SymbolScanner
{
public:
	bool sign = true;
	bool ifill = true;
	std::vector<std::string> _bineryConIns;
	void createMap(std::unordered_map<std::string, char>*);
	void assemble(std::string, std::string);
	void assembleInstruction();
	void print();
	void writeToFile();
	std::string twoComplement(std::string);
	std::string decimalToBinary(int);
	std::string convertBinToHex(std::string);
	std::string expansion(std::string, int);
	std::string rTypeIns(std::string arg_ins, std::istringstream& arg_remaining);
	std::string rTypeConvertor(int arg_digit);
	std::string iTypeIns(std::string arg_ins, std::istringstream& arg_remaining, int);
	std::string jTypeIns(std::string arg_ins, std::istringstream& arg_remaining);
};

