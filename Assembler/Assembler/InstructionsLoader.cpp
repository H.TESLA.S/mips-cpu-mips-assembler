#include "InstructionsLoader.h"
#include <fstream>
#include <iostream>

InstructionsLoader::InstructionsLoader()
{
	this->_NumberOfInstructions = 0;
	this->setOpCodes();
	this->setFounc();
}

void InstructionsLoader::loadInstructions(std::string arg_source, std::string arg_destination)
{
	this->fileDestination = arg_destination;
	std::ifstream source(arg_source);
	if (!source.is_open())
	{
		std::cout << "File Not Found" << std::endl;
		exit(1);
	}
	std::string line;
	while (std::getline(source, line))
	{
		std::size_t pos = line.find("#");
		if (pos != std::string::npos)
		{
			while (line[pos - 1] == ' ')
			{
				pos --;
			}
			line.erase(pos, line.length() - pos);
		}
		this->_NumberOfInstructions ++;
		this->_instructionVector.push_back(line);
	}
	source.close();
}
void InstructionsLoader::setOpCodes()
{
	this->__instruOpCode["and"] = "0000";
	this->__instruOpCode["or"] = "0000";
	this->__instruOpCode["add"] = "0000";
	this->__instruOpCode["sub"] = "0000";
	this->__instruOpCode["slt"] = "0000";
	this->__instruOpCode["jr"] = "0000";
	this->__instruOpCode["lw"] = "0001";
	this->__instruOpCode["sw"] = "0010";
	this->__instruOpCode["addi"] = "0011";
	this->__instruOpCode["beq"] = "0100";
	this->__instruOpCode["rol"] = "0101";
	this->__instruOpCode["j"] = "0110";
	this->__instruOpCode["jal"] = "0111";
	
}
void InstructionsLoader::setFounc()
{
	this->__rTypeFounc["and"] = "000000";
	this->__rTypeFounc["or"] = "000001";
	this->__rTypeFounc["add"] = "000010";
	this->__rTypeFounc["sub"] = "000011";
	this->__rTypeFounc["slt"] = "001000";
	this->__rTypeFounc["jr"] = "100000";
}
