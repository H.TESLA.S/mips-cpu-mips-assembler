#include "SymbolScanner.h"
#include "support.h"
#include <sstream>
#include <algorithm>
#include <cctype>

void SymbolScanner::fillSymbolTable()
{
	std::string firstWord;
	for (int i = 0; i < this->_instructionVector.size(); i++)
	{
		std::istringstream iss(this->_instructionVector[i]);
		iss >> firstWord;
		if (this->__instruOpCode.find(firstWord) == this->__instruOpCode.end())
		{
			if (this->_symbolTable.find(firstWord) != this->_symbolTable.end())
				exit(1);
			this->_symbolTable[firstWord] = i;
			int lenght = firstWord.length();
			while (this->_instructionVector[i][lenght + 1] == ' ')
			{
				lenght++;
			}
			this->_instructionVector[i].erase(0, lenght + 1);
		}
		std::string reg;
		std::vector<std::string> v;
		std::istringstream iss2(this->_instructionVector[i]);
		iss2 >> reg;
		while (std::getline(iss2, reg, ','))
		{
			reg.erase(std::remove(reg.begin(), reg.end(), '\t'), reg.end());
			v.push_back(reg);
		}
		for (auto itr = v.begin(); itr != v.end(); itr ++)
		{
			if (this->__instruOpCode.find(*itr) == this->__instruOpCode.end() and !std::isdigit(itr->front()))
			{
				this->_symbolPositions.push_back(i);
			}
		}
		v.clear();
	}
}

void SymbolScanner::placeValueOfSymbols()
{
	for (int position = 0; position < this->_symbolPositions.size(); position ++)
	{
		auto itr = this->_symbolTable.begin();
		bool flag = true;
		while (itr != this->_symbolTable.end())
		{
			auto pos = this->_instructionVector[this->_symbolPositions[position]].find(itr->first);
			if (pos != std::string::npos)
			{
				flag = false;
				this->_instructionVector[this->_symbolPositions[position]].erase(pos, itr->first.length());
				this->_instructionVector[this->_symbolPositions[position]].insert(pos, std::to_string(itr->second));
				itr++;
				continue;
			}
			itr ++;
		}
		if (flag)
			exit(1);
	}
}

void SymbolScanner::printIns()
{
	title();
	std::cout << "   [1]Instructions:" << std::endl;
	for (auto x : this->_instructionVector)
	{
		trim(x);
		std::cout << "\t" << x << std::endl;
	}
}
