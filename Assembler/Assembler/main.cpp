#include "InstructionsLoader.h"
#include "SymbolScanner.h"
#include "Assembler.h"
#include <string>
#include <iostream>
#include <windows.h>

int main(int argc, char* argv[])
{
	// This 6 Lines is to set Your Console Size The Right One
	HWND console = GetConsoleWindow();
	RECT r;
	GetWindowRect(console, &r);
	MoveWindow(console, r.left, r.top, 1150, 600, TRUE);
	SetConsoleCP(CP_UTF8);
	SetConsoleOutputCP(CP_UTF8);
	// The End
	std::string source, destination;
	source = "instu.txt";
	destination = "obj.txt";
	Assembler con;
	con.assemble(source, destination);
	std::cout << "\t\t\t\t\t\tYou Have Your Obj File Ready!!\t\t\t" << std::endl;
	system("pause");
	return 0;
}