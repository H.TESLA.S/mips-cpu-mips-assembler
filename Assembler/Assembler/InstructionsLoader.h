#pragma once
#include <vector>
#include <string>
#include <map>
class InstructionsLoader
{
public:
	InstructionsLoader();
	std::string fileDestination;
	int _NumberOfInstructions;
	std::map<std::string, std::string> __instruOpCode;
	std::map<std::string, std::string> __rTypeFounc;
	std::vector<std::string> _instructionVector;
	std::vector<long long> _decimalDecode;
	std::vector<int> _symbolPositions;
	/** load Instructions from file line by line to vector _arrIns */
	void loadInstructions(std::string arg_source, std::string arg_destination);
	/** set each instruction its OP code and make a map of them */
	void setOpCodes();
	void setFounc();
};