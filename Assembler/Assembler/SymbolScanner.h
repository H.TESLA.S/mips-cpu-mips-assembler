#pragma once
#include <map>
#include <string>
#include "InstructionsLoader.h"
class SymbolScanner :
	public InstructionsLoader
{
public:

	std::map<std::string, int> _symbolTable;
	/**
	 * fill the Symbol table with the values of each
	 */
	void fillSymbolTable();
	/**
	 * remove the Instruction vector symbols and put their values
	 */
	void placeValueOfSymbols();
	void printIns();
};

